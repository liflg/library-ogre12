Website
=======
http://www.ogre3d.org/

License
=======
LGPL v2.1 (see the file source/COPYING)

Version
=======
1.2.5

Source
======
ogre-linux_osx-v1-2-5.tar.bz2 (sha256: 9c7a31d41b626ff276b975f52395d17ee561e2d1dbcf1f6698fcb25c154d2d33)

Requires
========
* CEGUI
* DevIL
* FreeType
* jpeg-turbo
* NVIDIA Cg Toolkit
* SDL1
* tiff
* zziplib
