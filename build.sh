#!/bin/bash

set -e

select_multiarchname()
{
    echo "Please select the desired architecture:"
    echo "1): i386-linux-gnu"
    echo "2): x86_64-linux-gnu"
    echo "3): armeabi-linux-android"
    echo "4): armeabi_v7a-linux-android"
    read SELECTEDOPTION
    if [ x"$SELECTEDOPTION" = x"1" ]; then
        MULTIARCHNAME=i386-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"2" ]; then
        MULTIARCHNAME=x86_64-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"3" ]; then
        MULTIARCHNAME=armeabi-linux-android
    elif [ x"$SELECTEDOPTION" = x"4" ]; then
        MULTIARCHNAME=armeabi_v7a-linux-android
    else
        echo "Invalid option selected!"
        select_multiarchname
    fi
}

linux_build()
{
    if [ x"$BUILDTYPE" = xdebug ]; then
        CFLAGS="$CFLAGS -ggdb"
    else
        CFLAGS="$CFLAGS"
    fi

    if [ -z "$OPTIMIZATION" ]; then
        CFLAGS="$CFLAGS -O2"
    else
        CFLAGS="$CFLAGS -O$OPTIMIZATION"
    fi

    mkdir "$BUILDDIR"
    rm -rf "$PREFIXDIR"

cat << EOF > $BUILDDIR/freetype-config
    #!/bin/bash
    for arg in "\$@"
    do
        case "\$arg" in
            "--cflags" )
               echo -I${PWD}/../library-freetype/${MULTIARCHNAME}/include/freetype2;;
            "--libs" )
               echo -L${PWD}/../library-freetype/${MULTIARCHNAME}/lib -lfreetype;;
            "--version" )
               # from source/docs/VERSION.DLL
               echo 17.3.11;;
            *)
               echo ERROR: \$arg is not supported;;
       esac
    done
EOF

chmod 755 "$BUILDDIR"/freetype-config
export FT2_CONFIG="$BUILDDIR"/freetype-config

cat << EOF > $BUILDDIR/sdl-config
    #!/bin/bash
    for arg in "\$@"
    do
        case "\$arg" in
            "--cflags" )
               echo -I${PWD}/../library-sdl1/${MULTIARCHNAME}/include/SDL -D_GNU_SOURCE=1 -D_REENTRANT;;
            "--libs" )
               echo -L${PWD}/../library-sdl1/${MULTIARCHNAME}/lib -lSDL -lpthread;;
            "--version" )
               # from source/docs/VERSION.DLL
               echo 1.2.15;;
            *)
               echo ERROR: \$arg is not supported;;
       esac
    done
EOF

chmod 755 "$BUILDDIR"/sdl-config
export SDL_CONFIG="$BUILDDIR"/sdl-config

    ( cd source
      patch -Np1 -i ../patches/ogre-nvparse_fix_missing_headers.patch)

    ( ZZIPLIBDIR="${PWD}/../library-zziplib/${MULTIARCHNAME}"
      export ZZIPLIB_LIBS="-L$ZZIPLIBDIR/lib -lzzip"
      export ZZIPLIB_CFLAGS="-I$ZZIPLIBDIR/include"
      export LDFLAGS="$LDFLAGS $ZZIPLIB_LIBS"
      SDL1DIR="${PWD}/../library-sdl1/${MULTIARCHNAME}"
      export SDL_LIBS="-L$SDL1DIR/lib"
      export SDL_CFLAGS="-I$SDL1DIR/include"
      DEVILDIR="${PWD}/../library-devil/${MULTIARCHNAME}"
      export CFLAGS="$CFLAGS -I$DEVILDIR/include"
      export LDFLAGS="$LDFLAGS -L$DEVILDIR/lib"
      TIFFDIR="${PWD}/../library-tiff/${MULTIARCHNAME}"
      export LDFLAGS="$LDFLAGS -L$TIFFDIR/lib/ -ltiff"
      JPEGDIR="${PWD}/../library-jpeg-turbo/${MULTIARCHNAME}"
      export LDFLAGS="$LDFLAGS -L$JPEGDIR/lib/ -ljpeg"
      NVCGDIR="${PWD}/../library-nvidia-cg-toolkit/${MULTIARCHNAME}"
      export LDFLAGS="$LDFLAGS -L$NVCGDIR/lib/"
      export CFLAGS="$CFLAGS -I$NVCGDIR/include"
      export CFLAGS="$CFLAGS -Wno-deprecated"
      CEGUI04DIR="${PWD}/../library-cegui04/${MULTIARCHNAME}"
      export CEGUI_CFLAGS="-I$CEGUI04DIR/include -I$CEGUI04DIR/include/CEGUI"
      export CEGUI_LIBS="-L$CEGUI04DIR/lib -lCEGUIBase"
      export LIBS="-lzzip"
      cd source
      ./configure CXXFLAGS="$CFLAGS" \
        --prefix="$PREFIXDIR" \
        --disable-static \
        --with-platform=SDL
      make -j $(nproc)
      make install
      rm -rf "$PREFIXDIR"/{bin,lib/*.la,lib/OGRE/*.la,lib/pkgconfig,share}
      chrpath -d "$PREFIXDIR"/lib/*.so
      chrpath -d "$PREFIXDIR"/lib/OGRE/*.so )

    # clean up afterwards
    ( cd source
      git clean -df .
      git checkout . )
}

if [ -z "$MULTIARCHNAME" ]; then
    echo "\$MULTIARCHNAME is not set!"
    select_multiarchname
fi

if [ -z "$BUILDDIR" ]; then
    BUILDDIR="$PWD/build_$MULTIARCHNAME"
fi

if [ -z "$PREFIXDIR" ]; then
    PREFIXDIR="$PWD/$MULTIARCHNAME"
fi

case "$MULTIARCHNAME" in
i386-linux-gnu)
    linux_build;;
x86_64-linux-gnu)
    linux_build;;
*)
    echo "$MULTIARCHNAME is not (yet) supported by this script."
    exit 1;;
esac

install -m664 source/COPYING "$PREFIXDIR"/lib/LICENSE-ogre12.txt

rm -rf "$BUILDDIR"

echo "Ogre for $MULTIARCHNAME is ready."
